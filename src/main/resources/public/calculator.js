const DECIMAL_PRECISION = 5;
var curActivityNum = 4;

// Calculates corresponding percent for a grade
function calculatePercent(curElmt) {
    var parentElmt = curElmt.parentElement;
    var numer = NaN;
    var denom = NaN;
    if (curElmt.className == "grade numerator") {
        numer = parseFloat(curElmt.value);
        siblings = parentElmt.children;
        for (var i = 0; i < siblings.length; i++) {
            if (siblings[i].className == "grade denominator") {
                denom = parseFloat(siblings[i].value);
                break;
            }
        }
    } else if (curElmt.className == "grade denominator") {
        denom = parseFloat(curElmt.value);
        siblings = parentElmt.children;
        for (var i = 0; i < siblings.length; i++) {
            if (siblings[i].className == "grade numerator") {
                numer = parseFloat(siblings[i].value);
                break;
            }
        }
    }
    var row = curElmt.closest("tr");
    var row_children = row.children;
    var percentElmt;
    for (var i = 0; i < row_children.length; i++) {
        if (row_children[i].className == "percent") {
            percentElmt = row_children[i];
            break;
        }
    }
    if (!isNaN(numer) && !isNaN(denom) && denom != 0) {
        var percent = (numer / denom).toFixed(DECIMAL_PRECISION);
        percentElmt.textContent = percent;
    } else {
        percentElmt.textContent = "";
    }
}

// When a grade is changed, calculates percent
var gradeElmts = document.getElementsByClassName("grade");
for (var i = 0; i < gradeElmts.length; i++) {
    gradeElmts[i].onkeyup = function() {
        calculatePercent(this);
    }
    // Using onchange b/c onkeyup does not execute function 
    // if focus no longer on input before releasing key
    gradeElmts[i].onchange = function() {
        calculatePercent(this);
    }
}

// When Mean button pressed, outputs mean
var meanBtn = document.getElementById("mean");
meanBtn.onclick = function calculateMean() {
    var sumGrades = 0.0;
    var numGrades = 0;
    var numerators = document.getElementsByClassName("numerator");
    var denominators = document.getElementsByClassName("denominator");
    var numActivities = numerators.length;
    for (var i = 0; i < numActivities; i++) {
        var numer = parseFloat(numerators[i].value);
        var denom = parseFloat(denominators[i].value);
        if (!isNaN(numer) && !isNaN(denom) && denom != 0) {
            sumGrades += numer / denom;
            numGrades++;
        }
    }
    var result = document.getElementById("result");
    if (numGrades != 0) {
        var mean = (sumGrades / numGrades).toFixed(DECIMAL_PRECISION);
        result.textContent = mean;    
    } else {
        result.textContent = "";
    }
}

// When Weighted button pressed, calculates weighted mean
var weightedBtn = document.getElementById("weighted");
weightedBtn.onclick = function calculateWeighted() {
    var sumGrades = 0.0;
    var totalWeights = 0.0;
    var weights = document.getElementsByClassName("weight");
    var numerators = document.getElementsByClassName("numerator");
    var denominators = document.getElementsByClassName("denominator");
    var numActivities = numerators.length;
    for (var i = 0; i < numActivities; i++) {
        var weight = parseFloat(weights[i].value);
        var numer = parseFloat(numerators[i].value);
        var denom = parseFloat(denominators[i].value);
        if (!isNaN(weight) && !isNaN(numer) && !isNaN(denom) && denom != 0) {
            sumGrades += weight * numer / denom;
            totalWeights += weight;
        }
    }
    var result = document.getElementById("result");
    if (totalWeights != 0) {
        var mean = (sumGrades / totalWeights).toFixed(DECIMAL_PRECISION);
        result.textContent = mean;
    } else {
        result.textContent = "";
    }
}

// Add a row to the table
var addRowBtn = document.getElementById("addRow");
addRowBtn.onclick = function addRow() {
    var table = document.getElementsByTagName("table");
    var tbody = table[0].children;
    var rows = tbody[0].children;
    var clonedRow;
    
    // Get and clone the first row of the table
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].className == "activity") {
            clonedRow = rows[i].cloneNode(true);
            break;
        }
    }

    // Format cloned row by changing activity number, and clearing inputs and percent
    clonedChildren = clonedRow.children;
    curActivityNum++;
    for (var i = 0; i < clonedChildren.length; i++) {
        if (clonedChildren[i].className == "activityName") {
            clonedChildren[i].textContent = "Activity " + curActivityNum.toString();
        } else if (clonedChildren[i].className == "shortName") {
            clonedChildren[i].textContent = "A" + curActivityNum.toString();
        } else if (clonedChildren[i].className == "percent") {
            clonedChildren[i].textContent = "";
        }
    }
    var inputs = clonedRow.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = NaN;
    }

    // Append new row to end of table
    var newRow = table[0].appendChild(clonedRow);

    // Output percent for new row
    var newGradeElmts = document.getElementsByClassName("grade");
    var newNumerator = newGradeElmts[newGradeElmts.length - 2];
    var newDenominator = newGradeElmts[newGradeElmts.length - 1];
    newNumerator.onkeyup = function() {
        calculatePercent(this);
    }
    newNumerator.onchange = function() {
        calculatePercent(this);
    }
    newDenominator.onkeyup = function() {
        calculatePercent(this);
    }
    newDenominator.onchange = function() {
        calculatePercent(this);
    }
}

// Deletes a row from the table
// Note: must have at least one activity row
var deleteRowBtn = document.getElementById("removeRow");
deleteRowBtn.onclick = function removeLastRow() {
    var table = document.getElementsByTagName("table")[0];
    var numRows = table.rows.length;
    if (numRows > 2) {
        table.deleteRow(-1);
        curActivityNum--;
    }
}